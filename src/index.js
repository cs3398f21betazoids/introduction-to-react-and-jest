import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Board from './Board.js';
import GameInfo from './GameInfo.js';


class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
      </div>
    );
  }
}
export default Game;
// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
